package u05lab.solution

object TicTacToe extends App {
  sealed trait Player{
    def other: Player = this match {case X() => O(); case _ => X()}
    override def toString: String = this match {case X() => "X"; case _ => "O"}
  }
  case class X() extends Player
  case class O() extends Player

  case class Mark(x: Double, y: Double, player: Player)
  type Board = List[Mark]
  type Game = List[Board]

  def find(board: Board, x: Double, y: Double): Option[Player] =
    board collectFirst {case Mark(`x`,`y`,p) => p}

  def placeAnyMark(board: Board, player: Player): Seq[Board] = {
    if (won(board,player.other)) Seq(board) else
      for (x <- 0 to 2; y <- 0 to 2; if find(board, x, y) isEmpty)
        yield Mark(x, y, player) :: board
  }

  def won(board: Board, player: Player): Boolean = {
    def isWinH(triple: List[Mark]) = ((triple map {_.x} distinct) size) == 1
    def isWinV(triple: List[Mark]) = ((triple map {_.y} distinct) size) == 1
    def isWinD(triple: List[Mark]) = (triple forall {m => m.x == m.y})
    def isWinD2(triple: List[Mark]) = (triple forall {m => m.x + m.y == 2})
    val b = board filter {m => m.player == player}  // drops other player
    (for {m1 <- b.toStream
          m2 <- b; if m2 != m1
          m3 <- b; if m3 != m2; if m3 != m1
          triple = List(m1, m2, m3)  // any three distinct marks
          if isWinH(triple) || isWinV(triple) || isWinD(triple) || isWinD2(triple)
    } yield triple).isDefinedAt(0) // a stream, hence, only first is computed
  }

  def computeAnyGame(player: Player, moves: Int): Stream[Game] = moves match {
    case 0 => Stream(List(List()))
    case _ => for {
      game <- computeAnyGame(player.other, moves - 1)
      board <- placeAnyMark(game.head, player)
    } yield if (won(board, player.other)) game else board::game
  }

  def printBoards(game: Seq[Board]): Unit =
    for (y <- 0 to 2; board <- game.reverse; x <- 0 to 2) {
      print(find(board, x, y) map (_.toString) getOrElse ("."))
      if (x == 2) { print(" "); if (board == game.head) println()}
    }

  val depth = 7
  computeAnyGame(X(),depth)
      .filter {g => won(g.head,X()) || won(g.head,O())}
      .foreach {g => printBoards(g); println()}
}